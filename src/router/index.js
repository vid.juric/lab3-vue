import { createRouter, createWebHistory } from "vue-router"

import Home from "../views/Home.vue"
import Page1 from "../views/Page1.vue"
import Page2 from "../views/Page2.vue"
import NotFound from "../views/NotFound.vue"

const routes = [{
    path: "/",
    name: "Home",
    component: Home,
}, {
    path: "/page1",
    name: "Page1",
    component: Page1,
}, {
    path: "/page2",
    name: "Page2",
    component: Page2,
}, {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound
}]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router;